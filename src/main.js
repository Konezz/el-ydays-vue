// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import * as firebase from 'firebase'
import router from './router'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import store from './store/index'
import DateFilter from './filters/date'
import AlertCmp from './components/shared/Alert.vue'
import EditMeetupDetailsDialog from './components/meetup/edit/EditMeetupDetailsDialog.vue'

Vue.use(Vuetify)
Vue.component('app-alert', AlertCmp)
Vue.component('app-edit-meetup-details-dialog', EditMeetupDetailsDialog)
Vue.config.productionTip = false

Vue.filter('date', DateFilter)
/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  render: h => h(App),
  created () {
    firebase.initializeApp({
      apiKey: 'AIzaSyBkBXzU0D4afsxQiIAf90hNN3LJ-yL-MJg',
      authDomain: 'easy-level.firebaseapp.com',
      databaseURL: 'https://easy-level.firebaseio.com',
      projectId: 'easy-level',
      storageBucket: 'easy-level.appspot.com'
    })
    firebase.auth().onAuthStateChanged((user) => {
      if (user) {
        console.log(user)
        this.$store.dispatch('autoSignIn', user)
      }
    })
    this.$store.dispatch('loadMeetups')
  }
})
