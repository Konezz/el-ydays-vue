import Vue from 'vue'
import Router from 'vue-router'
import Home from '@/components/Home'
import MeetUps from '@/components/meetup/MeetUps'
import CreateMeetup from '@/components/meetup/CreateMeetup'
import MeetUp from '@/components/meetup/MeetUp'
import SignIn from '@/components/user/SignIn'
import SignUp from '@/components/user/SignUp'
import Profile from '@/components/user/Profile'
import Posts from '@/components/forum/Posts'
import AuthGuard from './auth-guard'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Home',
      component: Home
    },
    {
      path: '/meetups',
      name: 'Meetups',
      component: MeetUps
    },
    {
      path: '/meetup/new',
      name: 'CreateMeetup',
      component: CreateMeetup,
      beforeEnter: AuthGuard
    },
    {
      path: '/meetups/:id',
      name: 'Meetup',
      props: true,
      component: MeetUp
    },
    {
      path: '/profile',
      name: 'Profile',
      component: Profile,
      beforeEnter: AuthGuard
    },
    {
      path: '/signin',
      name: 'SignIn',
      component: SignIn
    },
    {
      path: '/signup',
      name: 'SignUp',
      component: SignUp
    },
    {
      path: '/posts',
      name: 'Forum',
      component: Posts
    }
  ],
  mode: 'history'
})
